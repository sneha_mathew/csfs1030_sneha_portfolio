
import React, { useEffect, useState } from 'react'
import ProjectCard from "./ProjectCards";

import { Container, Row, Col } from "react-bootstrap";

import "../../style.css";
import "bootstrap/dist/css/bootstrap.min.css";
//import Tilt from "react-parallax-tilt";

import Particle from "../Particle";

import "../../style.css";
import "bootstrap/dist/css/bootstrap.min.css";



const Projects = () => {

  const [listing, setListing] = useState([])

  useEffect(() => {
      const getData = async () => {
          const response = await fetch('http://localhost:4002/project', {
              method: 'GET',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
          })
          const data = await response.json()
          setListing(data)
      }
      getData()
  })

  return (
    <Container fluid className="project-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          My Recent <strong className="purple">Works </strong>
        </h1>
        <p style={{ color: "white" }}>
          Here are a few projects I've worked on recently.
        </p>
        <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={ listing.map(data => data.image1)} 
              isBlog={false}
              title={ listing.map(data => data.title1)}
              description={ listing.map(data => data.content1)}
              link="https://images.unsplash.com/photo-1513031300226-c8fb12de9ade?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8cGhvdG9ncmFwaGVyfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
             imgPath={ listing.map(data => data.image2)} 
             isBlog={false}
             title={ listing.map(data => data.title2)}
             description={ listing.map(data => data.content2)}
             link="https://images.unsplash.com/photo-1577741314755-048d8525d31e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
            imgPath={ listing.map(data => data.image3)} 
            isBlog={false}
            title={ listing.map(data => data.title3)}
            description={ listing.map(data => data.content3)}
            link="https://images.unsplash.com/photo-1526772662000-3f88f10405ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80"
            />
          </Col>

         
        </Row>
        
       
      </Container>
    </Container>
  );
}

export default Projects;
