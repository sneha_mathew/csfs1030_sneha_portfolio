

import { Form, FormGroup, Col,Input,Button, Label, Container } from 'reactstrap'

import "../../style.css";
import "bootstrap/dist/css/bootstrap.min.css";

import React, { useEffect, useState } from 'react'

import Particle from "../Particle";


const EditResume = () => {
    const [qualification, setqualification] = useState("")
    const [experience, setexperience] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [language1, setlanguage1] = useState("")
    const [language2, setlanguage2] = useState("")
    const [language3, setlanguage3] = useState("")
    const [framework, setframeworks] = useState("")
  const [listing, setListing] = useState([])

  useEffect(() => {
      const getData = async () => {
          const response = await fetch('http://localhost:4002/resumelist', {
              method: 'GET',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
          })
          const data = await response.json()
          setListing(data)
      }
      getData()
  }, )
  const formSubmit = async event => {
    event.preventDefault()
    const response = await fetch('http://localhost:4002/editresume', {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({qualification,experience,phoneNumber,language1,language2,language3,framework})
    })
    const payload = await response.json()
    if (response.status >= 400) {
        alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
    } else {
        alert(`Updated Resume details`)
        
        
    }
}

  return (
    <Container>
         <Particle />
     
     
        <Form className="my-5" onSubmit={formSubmit}>
        <FormGroup row>
                <Label for="qualification" sm={2}>Qualification</Label>
                <Col sm={10}>
                <Input type="text" name="qualificaton" id="qualification" placeholder={listing.map(data => data.qualification)}  
                value={qualification} 
                onChange={e => setqualification(e.target.value) }/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label for="experience" sm={2}>Experience</Label>
                <Col sm={10}>
                <Input type="text" name="experience" id="experience" placeholder="Enter Experience" value={experience} onChange={e => setexperience(e.target.value)}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label for="phoneEntry" sm={2}>Phone Number</Label>
                <Col sm={10}>
                <Input type="phone" name="phoneNumber" id="phoneEntry" placeholder="Enter phone number" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label for="language" sm={2}>Language 1</Label>
                <Col sm={10}>
                <Input type="text" name="language1" id="language1" placeholder="Enter Language" value={language1} onChange={e => setlanguage1(e.target.value)}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label for="language" sm={2}>Language 2</Label>
                <Col sm={10}>
                <Input type="text" name="language2" id="language2" placeholder="Enter Language" value={language2} onChange={e => setlanguage2(e.target.value)}/>
                </Col>
            </FormGroup>
            <FormGroup row>
                <Label for="language" sm={2}>Language 3</Label>
                <Col sm={10}>
                <Input type="text" name="language3" id="language3" placeholder="Enter Language" value={language3} onChange={e => setlanguage3(e.target.value)}/>
                </Col>
            </FormGroup>

            <FormGroup row>
                <Label for="framework" sm={2}>Framework</Label>
                <Col sm={10}>
                <Input type="textarea" name="framework" id="framework"  value={framework} onChange={e => setframeworks(e.target.value)}/>
                </Col>
            </FormGroup>
            <FormGroup check row>
                <Col sm={{ size: 10, offset: 2 }}>
               
                <Button  color="primary">Submit</Button>
                </Col>
            </FormGroup>
        </Form>
    </Container>
  )
}

export default EditResume