
import { Container, Row, Col } from "react-bootstrap";
import myImg from "../../Assets/avatar.svg";
import "../../style.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Tilt from "react-parallax-tilt";
import React, { useEffect, useState } from 'react'

import Particle from "../Particle";


const About = () => {
  
  const [listing, setListing] = useState([])

  useEffect(() => {
      const getData = async () => {
          const response = await fetch('http://localhost:4002/resumelist', {
              method: 'GET',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
          })
          const data = await response.json()
          setListing(data)
      }
      getData()
  }, )

  
  
  return (
    <Container fluid className="home-about-section" id="about">
      <Container>
      <Particle />
        <Row>
          <Col md={8} className="home-about-description">
            <h1 style={{ fontSize: "2.6em" }}>
              LET ME <span className="purple"> INTRODUCE </span> MYSELF
            </h1>
            <p className="home-about-body">
            I Have <b className="purple"> { listing.map(data => <tr>{data.qualification}  </tr>)
                    } </b>
              <br />
              
              <br />I am fluent in classics like
              <i>
                <b className="purple"> { listing.map(data => <tr>{data.language1}  {data.language2}  {data.language3} </tr>)
                    } </b>
              </i>
              <br />
              <br />
              My current focus is on becoming a &nbsp;
              <i>
                <b className="purple">
                { listing.map(data => <tr>{data.experience}  </tr>)
                    }
                </b>
              </i>
              <br />
              <br />
              Whenever possible, my passion for developing products
              with
              <i>
                <b className="purple"> Modern Javascript Frameworks</b>
              </i>
                &nbsp; like
              <i>
                <b className="purple">  { listing.map(data => <tr>{data.framework}  </tr>)
                    }</b>
              </i>
            </p>
          </Col>
          <Col md={4} className="myAvtar">
            <Tilt>
              <img src={myImg} className="img-fluid" alt="avatar" />
            </Tilt>
          </Col>
        </Row>
        
      </Container>
    </Container>
  );
}
export default About;


