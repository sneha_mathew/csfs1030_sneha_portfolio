import React, { useState } from 'react'
import { Form, FormGroup, Col,Input,Button, Label, Container } from 'reactstrap'

import "../../style.css";

import 'font-awesome/css/font-awesome.min.css';

import Particle from "../Particle";
const AddProject = () => {
    const [title1, settitle1] = useState("")
    const [title2, settitle2] = useState("")
    const [title3, settitle3] = useState("")
    const [image1, setimage1] = useState("")
    const [image2, setimage2] = useState("")
    const [image3, setimage3] = useState("")
    const [content1, setcontent1] = useState("")
    const [content2, setcontent2] = useState("")
    const [content3, setcontent3] = useState("")

    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4002/addproject', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({title1,image1,content1,title2,image2,content2,title3,image3,content3})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
        } else {
            alert(`Added project details`)
            
            settitle1("");
            setimage1("");
            setcontent1("");
            settitle2("");
            setimage2("");
            setcontent2("");
            settitle3("");
            setimage3("");
            setcontent3("");
        }
    }

    return (
        <Container>
             <Particle />
         
         
            <Form className="my-5" onSubmit={formSubmit}>
            
            <FormGroup row>
                    <Label for="title1" sm={2}>Title of First Project</Label>
                    <Col sm={10}>
                    <Input type="text" name="title1" id="title1" placeholder="Enter project title1"  required value={title1} onChange={e => settitle1(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="image1" sm={2}>Image link1 </Label>
                    <Col sm={10}>
                    <Input type="text" name="image1" id="image1" placeholder="Add image1" value={image1} onChange={e => setimage1(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="content1" sm={2}>Project Description1</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="content1" id="content1" placeholder="Enter Project Description1" value={content1} onChange={e => setcontent1(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="title2" sm={2}>Title of Second Project</Label>
                    <Col sm={10}>
                    <Input type="text" name="title2" id="title2" placeholder="Enter project title2"  required value={title2} onChange={e => settitle2(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="image2" sm={2}>Image link2 </Label>
                    <Col sm={10}>
                    <Input type="text" name="image2" id="image2" placeholder="Add image2" value={image2} onChange={e => setimage2(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="content2" sm={2}>Project Description2</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="content2" id="content2" placeholder="Enter Project Description2" value={content2} onChange={e => setcontent2(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="title3" sm={2}>Title for third Project</Label>
                    <Col sm={10}>
                    <Input type="text" name="title3" id="title3" placeholder="Enter project title3"  required value={title3} onChange={e => settitle3(e.target.value) }/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="image3" sm={2}>Image link3 </Label>
                    <Col sm={10}>
                    <Input type="text" name="image3" id="image3" placeholder="Add image3" value={image3} onChange={e => setimage3(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="content3" sm={2}>Project Description3</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="content3" id="content3" placeholder="Enter Project Description3" value={content3} onChange={e => setcontent3(e.target.value)}/>
                    </Col>
                </FormGroup>
                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>
                   
                    <Button  color="primary">Submit</Button>
                    </Col>
                </FormGroup> 
            </Form>
        </Container>
      )
    }

    export default AddProject