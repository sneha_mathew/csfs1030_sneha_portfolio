

import cors from 'cors'

import addContact from './src/routes/contact'
import users from './src/routes/users'
import userRouter from './src/routes/userroute'
import contactList from './src/routes/contactlist'
import express from "express";
import addResume from './src/routes/resume'
import resumeList from './src/routes/resumelist'
import addProject from './src/routes/addproject'
import viewProject from './src/routes/viewproject'

let jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const app = express()
const PORT = process.env.PORT || 3000

app.use(express.json())
app.use(cors())

app.use(users);
app.use(addContact);
app.use(userRouter);
app.use(addResume);
app.use(contactList)
app.use(resumeList);
app.use(addProject);
app.use(viewProject);
app.listen(PORT, () => {
    console.log(`Server started at http://localhost:${PORT}`)
})
