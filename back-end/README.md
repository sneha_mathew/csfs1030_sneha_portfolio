# BackEnd

## To get the project running: 


1. Run `$ npm install` in the "back-end" directory to install dependencies

2. Creat a .env file in the same location as index.js with 
PORT=
JWT_SECRET=
DB_SECRET = 
DB_NAME = 

3. Initialize the React application by running `$ npm start` in the "back-end" directory

## SQL Query information

|Name |File Path |Route Description |Query Line No.|
--- | --- | --- | ---
|Sneha|back-end/src/routes/addproject.js|Query to insert project details into "project" table|11|
|Sneha|back-end/src/routes/contact.js|Query to insert contacted details into "contact" table|9|
|Sneha|back-end/src/routes/contactlist.js|Query to get all the contacters information from "contact" table|8|
|Sneha|back-end/src/routes/resume.js|Query to insert resume details into "resume" table|11|
|Sneha|back-end/src/routes/resumelist.js|Query to get all the resume details from "resume" table|11|
|Sneha|back-end/src/routes/userroute.js|Query to fetch the user exist in the db based on the email "login"|25|
|Sneha|back-end/src/routes/users.js|Query to create new user "login" table|18|
|Sneha|back-end/src/routes/viewproject.js|Query to view all the project details based on the project id "project" table|11|