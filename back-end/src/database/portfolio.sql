create database portfolio;

use portfolio;

CREATE table IF NOT EXISTS contact(
name varchar(255) not null,
email VARCHAR(255) NOT NULL,
phoneNumber VARCHAR(255) NOT NULL,
content VARCHAR(255) NOT NULL,
PRIMARY KEY(email));
   
   insert into contact(name,email,phoneNumber,content) values
   ("tester","tester@gmail.com","515263627","need to build a website");



CREATE table IF NOT EXISTS login(
email varchar(255) not null,
name varchar(255) not null,
password VARCHAR(255) NOT NULL,
PRIMARY KEY(email));

    insert into login(email,name,password) value 
    ("sneha@gmail.com","Sneha Mathew","password");




CREATE table IF NOT EXISTS resume(
resumeid INT auto_increment NOT NULL,
qualification varchar(255) not null,
experience varchar(255) not null,
phoneNumber varchar(255) not null,
language1 varchar(255) not null,
language2 varchar(255) not null,
language3 varchar(255) not null,
framework VARCHAR(255) NOT NULL,
PRIMARY KEY(resumeid));

     insert into resume(qualification,experience,phoneNumber,language1,language2,language3,framework) values
   ("Masters in Computer Application","Front End Developer","515263627","C++","Java","Javascript","Nodejs and React");
   



CREATE table IF NOT EXISTS project(
projectid INT auto_increment NOT NULL,
title1 varchar(255) not null,
title2 varchar(255) not null,
title3 varchar(255) not null,
image1 varchar(255) not null,
image2 varchar(255) not null,
image3 varchar(255) not null,
content1 varchar(255) not null,
content2 varchar(255) not null,
content3 varchar(255) not null,
PRIMARY KEY(projectid));

    insert into project(title1,title2,title3,image1,image2,image3,content1,content2,content3) value 
("Photography Blog","Gamer.io","Travel","https://images.unsplash.com/photo-1513031300226-c8fb12de9ade?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8cGhvdG9ncmFwaGVyfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
"https://images.unsplash.com/photo-1577741314755-048d8525d31e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80",
"https://images.unsplash.com/photo-1526772662000-3f88f10405ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80",
"Website publishing a blog, or running a business,  proven to be the most beautiful way to present the photographers ideas online.",
"Web site for Gamers",
"Travel Blog website with all the Historical details");


