import express from 'express'
import connection from '../database/connection.js'


const addContact = express.Router()

addContact.post("/contact", (req, res) => {
    
        connection.query("INSERT INTO contact(name,email,phoneNumber,content) VALUES ( ?, ?, ?, ? )", [req.body.name, req.body.email, req.body.phoneNumber, req.body.content], function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        });
    

});

export default addContact;

