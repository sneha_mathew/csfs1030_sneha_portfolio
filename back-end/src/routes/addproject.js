import express from 'express'

import connection from '../database/connection.js'



const addProject = express.Router()

addProject.post("/addproject", (req, res) => {
   
        connection.query("INSERT INTO project(title1,image1,content1,title2,image2,content2,title3,image3,content3) VALUES ( ?,?,?,?,?,?,?,?,?)",
         [req.body.title1,req.body.image1,req.body.content1,req.body.title2,req.body.image2,req.body.content2,req.body.title3,req.body.image3,req.body.content3],
          function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        });
    

});

export default addProject;

