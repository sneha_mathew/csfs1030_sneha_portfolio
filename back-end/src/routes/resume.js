import express from 'express'

import connection from '../database/connection.js'



const addResume = express.Router()

addResume.post("/resume", (req, res) => {

        connection.query("INSERT INTO resume(qualification,experience,phoneNumber,language1,language2,language3,framework) VALUES ( ?,?,?,?,?,?,?)",
         [req.body.qualification,req.body.experience,req.body.phoneNumber,req.body.language1,req.body.language2,req.body.language3,req.body.framework], function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        });
    

});
/*addResume.put("/editresume", (req, res) => {

    connection.query(
        "UPDATE resume SET qualification = ?, experience = ?, phoneNumber = ?, language1 = ?, language2 = ?, language3 = ?,framework = ? WHERE resumeid=2}",
        [req.body.qualification,req.body.experience,req.body.phoneNumber,req.body.language1,req.body.language2,req.body.language3,req.body.framework], function (error, results, fields) {
            if (error) throw error;
            return res.status(201).send(results);
        });
    

});
*/
export default addResume;

